
if [ "$METHOD" == "" ]; then
    METHOD="clapack"
fi

export OPENBLAS_NUM_THREADS=$NUM_THREADS
export USE_THREAD=1

echo "{% num_threads %}{% ${NUM_THREADS} %}"
echo "{% matrix_size %}{% ${MATRIX_SIZE} %}"
# echo "{% total_memory %}{% $(/usr/sbin/dmidecode -t 17 | grep 'Size.*MB' | awk '{s+=$2} END {print s / 1024}') %}"

if [ "$METHOD" == "clapack" ]; then
    echo "Method: CLPACK/LAPACKE"
    gcc -lblas -llapack -llapacke -lopenblas -lpthread -o cholesky_c_lapack_lv3 cholesky_c_lapack_lv3.cpp
    ./cholesky_c_lapack_lv3 $MATRIX_SIZE
elif [ "$METHOD" == "armadillo" ]; then
    echo "Method: Armadillo"
    g++ cholesky_armadillo.cpp -o cholesky_armadillo -O3 -larmadillo -llapack -lblas 
    ./cholesky_armadillo $MATRIX_SIZE
elif [ "$METHOD" == "custom" ]; then
    echo "Method: Naive implementation"
    g++ -o cholesky_blas_lv1 cholesky_blas_lv1.cpp
    ./cholesky_blas_lv1 $MATRIX_SIZE
fi

[ -f "/mnt/workspace/scripts/custom_script_post.sh" ] && bash /mnt/workspace/scripts/custom_script_post.sh