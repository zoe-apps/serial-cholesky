#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

typedef double** MAT;

/* In-place Cholesky decomposition for L in A = L L^T where L is a
   lower triangular matrix.  Uses the Cholesky-Crout algorithm,
   based on Stoer & Bulirsch "Introduction to numerical analysis"
   Sect. 4.3, but with the loops for i = j and i != j separated and
   changed to access only the lower triangular part of the matrix.
   Output is computed column by column so the accesses in the
   innermost loop (indexed by k) are sequential in memory. */

int cholesky (double **a, int n) {
  double x, r;
  int i, j, k;

  /* Loop over columns */
  for(j = 0; j < n; j++) {
    /* i = j */
    x = a[j][j];  /* A_jj */

    for(k = 0; k < j; k++)
      x -= a[j][k] * a[j][k];  /* L_jk L_jk */

    if(x < 0)
      return(-1);

    x = sqrt(x);

    a[j][j] = x;  /* L_jj */
    r = 1.0 / x;

    /* i != j */
    for(i = j+1; i < n; i++) {
      x = a[i][j];  /* A_ij */

      for(k = 0; k < j; k++)
        x -= a[i][k] * a[j][k];  /* L_ik L_ij */

      a[i][j] = x * r;  /* L_ij = x / L_jj */
    }
  }

  return(0);
}

//print a square real matrix A of size n with caption s
//(n items per line).
void print_mat(char *s, int n, MAT A)
{
    int i, j;
    printf("\n %s\n", s);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
            printf(" %10.6f", A[i][j]);
        printf("\n");
    }
}

// main program to demonstrate the use of function cholsl()
int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("\nNo Extra Command Line Argument Passed Other Than Program Name\n");
        return 1;
    }

    int n = atoi(argv[1]);

    MAT A;
    int i, j;

    printf("\n Size = %d\n", n);

    clock_t tStart = clock();

    // define lower half of symmetrical matrix
    A = new double*[n];

    if(A == NULL)
    {
        printf("Memory allocation failed");
        return 1;
    }

    srand(time(0));

    for (int i = 0; i < n; ++i){
        A[i] = new double[n];
        for (int j = 0; j <= i; ++j){
            double r = (double)rand() / RAND_MAX;
            A[i][j] = r;
            if (i == j){
                A[i][j] += n;
            }
        }
    }
    for (int i = 0; i < n; ++i){
        for (int j = i + 1; j < n; ++j){
            A[i][j] = A[j][i];
        }
    }

    printf("{%% generating_matrix %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    if (n < 10) { print_mat("Matrix A:", n, A);}

    tStart = clock();
    cholesky(A, n);
    printf("{%% decompose_matrix %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    if (n < 10) { print_mat("Matrix L:", n, A); }
        
    printf("\n");
    return 0;
}
