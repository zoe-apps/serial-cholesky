#define ARMA_DONT_USE_WRAPPER
#define ARMA_USE_LAPACK
#define ARMA_USE_BLAS


#include <iostream>
#include <armadillo>
#include <stdlib.h>
#include <time.h>
#include <chrono>

using namespace std;
using namespace arma;

// compile this code by:
// g++ cholesky_armadillo.cpp -o cholesky_armadillo -O2 -larmadillo -lblas -llapack
// or: 
// g++ cholesky_armadillo.cpp -o cholesky_armadillo -O2 -larmadillo -framework Accelerate    (if on MacOS)

template<typename TimeT = std::chrono::microseconds, 
    typename ClockT=std::chrono::high_resolution_clock,
    typename DurationT=double>
class Stopwatch
{
private:
    std::chrono::time_point<ClockT> _start, _end;
public:
    Stopwatch() { start(); }
    void start() { _start = _end = ClockT::now(); }
    DurationT stop() { _end = ClockT::now(); return elapsed();}
    DurationT elapsed() { 
        auto delta = std::chrono::duration_cast<TimeT>(_end-_start);
        return delta.count(); 
    }
};

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("\nNo Extra Command Line Argument Passed Other Than Program Name\n");
        return 1;
    }

    struct timespec start, finish;
    double elapsed;

    int n = atoi(argv[1]);
    clock_t tStart = clock();
    srand(time(0));
    mat A = zeros<mat>(n, n);
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j <= i; ++j)
        {
            double r = (double)rand() / RAND_MAX;
            A(i, j) = A(j, i) = r;
            if (i == j)
            {
                A(i, j) += n;
            }
        }
    }
    printf("{%% generating_matrix %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
    tStart = clock();
    // clock_gettime(CLOCK_MONOTONIC, &start);
    Stopwatch<> decomposing_sw;
    mat B = chol(A);
    decomposing_sw.stop();

    // clock_gettime(CLOCK_MONOTONIC, &finish);
    // elapsed = (finish.tv_sec - start.tv_sec);
    // elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    printf("{%% decomposing_matrix %%}{%% %.9f %%}\n\n", decomposing_sw.elapsed()/1e6);

    printf("{%% total_process_time %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    if (n <= 10)
    {
        cout << A << endl
             << B << endl;
    }
    return 0;
}