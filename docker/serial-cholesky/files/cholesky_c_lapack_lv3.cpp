#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

/* If the LAPACK library and/or headers are not in your compiler's include path,
   make sure to use the compiler's -I and -L options to specify where your
   compiler should look for them.*/
#include <lapacke.h>
#include <cblas.h>

/* Prints matrix in column-major format. */
static void show_matrix(const double *A, const int n)
{
    int i = 0, j = 0;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("%2.5f ", A[i * n + j]);
        }
        printf("\n");
    }
}

/* Zeros out the lower diagonal portion of a matrix. */
static void zero_ldiag(double *m, const int n)
{
    int i = 0, j = 0;
    for (i = 0; i < n; i++)
    {
        for (j = i; j < n; j++)
        {
            m[j * n + i] = 0.0;
        }
    }
}

int main(int argc, char *argv[])
{
    if(argc==1){
        printf("\nNo Extra Command Line Argument Passed Other Than Program Name\n");
        return 1;
    }

    int N = atoi(argv[1]);
    printf("N=%d\n", N);
    //printf("We can allocate maximum %u bytes continuously\n", SIZE_MAX);
    double *a;

    clock_t tStart = clock();
    // a = (double *)calloc((unsigned long)(N * N), sizeof(double));

    const size_t alignment = 16;  // change the 16 to 64 if you wish to align to the cache line
    
    int status = posix_memalign((void **)&a, ( (alignment >= sizeof(void*)) ? alignment : sizeof(void*) ), sizeof(double)*N*N);
    
    a = (status == 0) ? a : NULL;

    if(a == NULL)
    {
        printf("Memory allocation failed");
        return 1;
    }

    srand(time(0));

    for (int i = 0; i < N * N; ++i)
    {
        a[i] = (double)rand() / RAND_MAX;
    }

    double tmp = 0;
    for (int i = 0; i < N; ++i)
    {
        for (int j = i; j < N; ++j)
        {
            tmp = (a[i * N + j] + a[j * N + i]) / 2;
            if (i == j)
            {
                tmp += N;
            }
            a[i * N + j] = a[j * N + i] = tmp;
        }
    }

    printf("{%% generating_matrix %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    if (N <= 50){
        printf("The covariance matrix:\n\n");
        show_matrix(a, N);
    }

    char uplo = 'L'; /* We ask LAPACK for the lower diagonal matrix L */
    int info = 0;    /* "Info" return value, used for error-checking */
    int lda = N;     /* Leading dimension of array - see above */

    tStart = clock();

    /* LAPACK API uses updating of input variables instead of returning */
    dpotrf_(&uplo, &N, a, &lda, &info);

    /* Will be 0 IFF call succeeds */
    assert(info == 0);

    /* Note that because m1 is over-written byy LAPACK, you need to 0 out the 
       lower diagonal entries yourself, since LAPACK will not do it for you. */
    zero_ldiag(a, N);

    printf("{%% decompose_matrix %%}{%% %.9f %%}\n\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

    if (N <= 50){
        printf("Factorized matrix:\n");
        show_matrix(a, N);
    }

    free(a);
}