#!/usr/bin/bash

######################################################
# This image is configured to support running:
#    - CLPACK / Armadillo
######################################################

sudo apt-get update

sudo apt-get install -y --force-yes --no-install-recommends \
    apt-utils \
    build-essential \
    libblas-dev \
    liblapack3 \
    libboost-dev \
    liblapack-dev \
    libopenblas-base \
    libopenblas-dev \
    libatlas-base-dev \
    liblapacke-dev \
    pkg-config \
    zip \
    g++ \
    zlib1g-dev \
    wget \
    git \
    vim \
    && sudo apt-get clean

sudo apt-get update

sudo apt-get install -y git
git clone https://github.com/xianyi/OpenBlas.git
cd OpenBlas/
make clean
make -j4 BINARY=64
sudo mkdir /usr/OpenBLAS
sudo chmod o+w,g+w /usr/OpenBLAS/
sudo make PREFIX=/usr/OpenBLAS install
sudo ldconfig
sudo ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so
sudo ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so.3
sudo ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/libblas.so.3.5
sudo ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so
sudo ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so.3
sudo ln -sf /usr/OpenBLAS/lib/libopenblas.so /usr/liblapack.so.3.5

sudo apt-get install -y --force-yes libarmadillo-dev







